#define MINIAUDIO_IMPLEMENTATION

#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <vector>

#include "miniaudio.h"
#include "opus.h"
#include "opus_decoder.h"
#include "opus_encoder.h"

const constexpr ma_format kFormat = ma_format_s16;
const constexpr uint8_t kChannels = 2;
const constexpr uint16_t kSampleRate = 48000;

class MiniAudioCapturer {
public:
    using AudioData = std::vector<uint8_t>;
    using AudioHandler = std::function<void(std::shared_ptr<AudioData>)>;

public:
    MiniAudioCapturer(AudioHandler handler) : handler_(std::move(handler)) {
        config_ = ma_device_config_init(ma_device_type_capture);
        config_.capture.format = kFormat;
        config_.capture.channels = kChannels;
        config_.sampleRate = kSampleRate;
        config_.dataCallback = &MiniAudioCapturer::OnAudio;
        config_.pUserData = this;
    }
    ~MiniAudioCapturer() {
        ma_device_uninit(&device_);
    }

    void Start() {
        if (ma_device_init(nullptr, &config_, &device_) != MA_SUCCESS) {
            std::cout << "error on init" << std::endl;
        }
        std::cout << "Device Name = " << device_.capture.name << std::endl;
        if (ma_device_start(&device_) != MA_SUCCESS) { std::cout << "error on start" << std::endl; }
    }

private:
    void static OnAudio(ma_device *device, void *output, const void *input, ma_uint32 frame_count) {
        auto thiz = reinterpret_cast<MiniAudioCapturer *>(device->pUserData);
        auto data = std::make_shared<AudioData>();  // todo hand all
        uint16_t channels = thiz->config_.capture.channels;
        uint16_t per_bytes = thiz->config_.capture.format;
        if (per_bytes > 4) { per_bytes = 4; }
        data->resize(frame_count * channels * per_bytes);
        std::cout << frame_count << std::endl;
        memcpy(data->data(), input, data->size());
        thiz->handler_(data);
    };

private:
    AudioHandler handler_;
    ma_device_config config_;
    ma_device device_;
};

int main(int argc, char **argv) {
    codec::OEncoder encoder_(kSampleRate, kChannels);
    codec::ODecoder decoder_(kSampleRate, kChannels);

    std::ofstream pcm("hello.pcm", std::ios::binary);
    std::ofstream encoded("hello.encoded", std::ios::binary);
    std::ofstream decoded("hello.decoded", std::ios::binary);
    MiniAudioCapturer cap([&](std::shared_ptr<MiniAudioCapturer::AudioData> data) {
        pcm.write(reinterpret_cast<char *>(data->data()), data->size());
        std::cout << data->size() << std::endl;
        auto encoded_data = encoder_.Encode(data);
        encoded.write(reinterpret_cast<char *>(encoded_data->data()), encoded_data->size());
        std::cout << "encoded_data.size() = " << encoded_data->size() << std::endl;
        auto decoded_data = decoder_.Decode(encoded_data);
        decoded.write(reinterpret_cast<char *>(decoded_data->data()), decoded_data->size());
        std::cout << "1:" << data->size() << std::endl;
        std::cout << "2:" << encoded_data->size() << std::endl;
        std::cout << "3:" << decoded_data->size() << std::endl;
        std::cout << "4:" << std::endl;
    });
    cap.Start();
    getchar();

    return 0;
}
